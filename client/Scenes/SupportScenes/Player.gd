extends PlayerClass
class_name Player


##
# export vars
##

##
# script vars
##


##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	_send_current_position()
	
func _physics_process(delta: float) -> void:
	pass

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == BUTTON_LEFT:
				rpc_id(1, "rpc_send_left_mouseclick", position)
			if event.button_index == BUTTON_RIGHT:
				rpc_id(1, "rpc_send_right_mouseclick", position)

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func _send_current_position() -> void:
	# send the current mouse pointer position to game server
	rpc_id(1,"rpc_send_current_position", get_global_mouse_position())
