extends Node2D
class_name WorldScene


##
# export vars
##

##
# script vars
##

onready var players_node : Node = $Players
onready var objects_node : Node = $Objects

onready var player_scene : PackedScene = preload("res://Scenes/SupportScenes/Player.tscn")
onready var peer_player_scene : PackedScene = preload("res://Scenes/SupportScenes/PeerPlayer.tscn")
onready var object_scene : PackedScene = preload("res://Scenes/SupportScenes/Object.tscn")
onready var own_peer_id : int = Server.network.get_unique_id()

##
# signals
##

##
# default functions
##

func _init() -> void:
	if custom_multiplayer == null:
		custom_multiplayer = Server.custom_multiplayer

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func _spawn_self() -> void:
	# spawns the own player object
	
	var player : Node2D = player_scene.instance()
	player.name = str(own_peer_id)
	players_node.add_child(player)
	player.set_username(PlayerData.username)
	

puppet func rpc_despawn_player(rpc_id: int) -> void:
	# despawn player
	Logger.debug(self, "Received despawn player call for id {id}".format({"id":rpc_id}))
	
	var player : Node2D = players_node.get_node(str(rpc_id))
	var players_objects : Node = objects_node.get_node(str(rpc_id))

	if player:
		player.call_deferred("queue_free")
		players_node.remove_child(player)
		
	if players_objects:
		players_objects.call_deferred("queue_free")
		objects_node.remove_child(players_objects)
	
puppet func rpc_spawn_player(rpc_id: int) -> void:
	# spawns peer player
	Logger.debug(self, "Received spawn player call for id {id}".format({"id":rpc_id}))
	
	if not players_node.get_node(str(rpc_id)):
		var player : Node2D 
		if rpc_id == own_peer_id:
			player = player_scene.instance()
		else:
			player = peer_player_scene.instance()
		
		player.name = str(rpc_id)
		players_node.add_child(player)

puppet func rpc_spawn_player_object(owner_id: int, object_name: String, pos: Vector2) -> void:
	# spawn in objects
	
	Logger.debug(self, "Received player object spawn instruction for owner {id}".format({"id":owner_id}))
	
	var player = players_node.get_node(str(owner_id))
	
	if not player:
		Logger.warning(self, "Unable to find owner with id {id}".format({"id": owner_id}))
		return
	
	if not objects_node.get_node(str(owner_id)):
		var n : Node = Node.new()
		n.name = str(owner_id)
		objects_node.add_child(n, true)
		
	if not objects_node.get_node(str(owner_id)).get_node(object_name):
		var o : Node2D = object_scene.instance()
		o.name = object_name
		objects_node.get_node(str(owner_id)).add_child(o,true)
		o.position = pos
		o.star.modulate = player.pointer.modulate

puppet func rpc_despawn_player_object(owner_id: int, object_name: String) -> void:
	# despawn objects spawned by players
	
	Logger.debug(self, "Received player object despawn instruction for owner {id}".format({"id":owner_id}))

	var object : Node2D = objects_node.get_node(str(owner_id)).get_node(object_name)
	
	if object:
		object.call_deferred("queue_free")
		objects_node.get_node(str(owner_id)).remove_child(object)
		
