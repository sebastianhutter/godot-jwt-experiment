extends Node2D
class_name Player


##
# export vars
##

##
# script vars
##

onready var username_label : Label = $Username

##
# signals
##

signal left_mouseclick
signal right_mouseclick

##
# default functions
##

func _init() -> void:
	if custom_multiplayer == null:
		custom_multiplayer = Server.custom_multiplayer

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	rpc_id(0, "rpc_update_position", position)
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func set_username(u: String) -> void:
	# sets the username label
	username_label.text = u

func _update_position(pos: Vector2) -> void:
	# update the position on the server and send updated
	# position to all connected peers
	
	position = pos

master func rpc_initialize_player() -> void:
	rpc_id(0, "rpc_update_username", username_label.text)

master func rpc_send_current_position(pos: Vector2) -> void:
	# receive current position of player scenes
	
	var rpc_id = custom_multiplayer.get_rpc_sender_id()
	_update_position(pos)
	
master func rpc_send_left_mouseclick(pos: Vector2) -> void:
	# left mouse click received from peer
	
	var rpc_id = custom_multiplayer.get_rpc_sender_id()
	Logger.debug(self, "Received left mouse click event form {id}".format({"id":rpc_id}))
	emit_signal("left_mouseclick", rpc_id, pos)

master func rpc_send_right_mouseclick(pos: Vector2) -> void:
	# left mouse click received from peer
	
	var rpc_id = custom_multiplayer.get_rpc_sender_id()
	Logger.debug(self, "Received right mouse click event form {id}".format({"id":rpc_id}))
	emit_signal("right_mouseclick", rpc_id, pos)
