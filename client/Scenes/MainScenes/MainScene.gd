extends Node
class_name Client


##
# export vars
##

##
# script vars
##

onready var world_scene : Resource = preload("res://Scenes/MainScenes/WorldScene.tscn")
onready var login_screen : Control = get_node("LoginScreen")
##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	login_screen.connect("access_granted", self, "load_worldscene")
	
func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func load_worldscene() -> void:
	# unload the login box and load the worldscene
	
	# Remove the current level
	var ls : Control = get_node("LoginScreen")
	remove_child(ls)
	ls.call_deferred("free")
	
	add_child(world_scene.instance())

