"""Python Flask API Auth0 integration example
"""

from functools import wraps
import json
from os import environ as env
from typing import Dict

from six.moves.urllib.request import urlopen

from dotenv import load_dotenv, find_dotenv
from flask import Flask, request, jsonify, _request_ctx_stack, Response, abort
from flask_cors import cross_origin
from jose import jwt

from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import datetime
import time

ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)
AUTH0_DOMAIN = env.get("AUTH0_DOMAIN")
API_IDENTIFIER = env.get("AUTH0_AUDIENCE_GODOT_DATABASE")
ALGORITHMS = ["RS256"]

APP = Flask(__name__)

# Create the SqlAlchemy db instance
APP.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://{user}:{pwd}@{host}/{db}".format(
    user=env.get("MYSQL_USER"),
    pwd=env.get("MYSQL_PASSWORD"),
    host="db",
    db=env.get("MYSQL_DATABASE"),
) 
APP.config["SQLALCHEMY_ECHO"] = True
APP.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(APP)

# Initialize Marshmallow
ma = Marshmallow(APP)

# db schema and json model for logins table
class Logins(db.Model):
    __tablename__ = "logins"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    signature = db.Column(db.String(512))
    last_auth = db.Column(db.Integer, default=0)
    last_login = db.Column(db.Integer, default=0)
    auth_count =  db.Column(db.Integer, default=0)
    login_count =  db.Column(db.Integer, default=0)

class LoginsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        load_instance = True
        model = Logins
        sqla_session = db.session

# Format error response and append status code.
class AuthError(Exception):
    """
    An AuthError is raised whenever the authentication failed.
    """
    def __init__(self, error: Dict[str, str], status_code: int):
        super().__init__()
        self.error = error
        self.status_code = status_code


@APP.errorhandler(AuthError)
def handle_auth_error(ex: AuthError) -> Response:
    """
    serializes the given AuthError as json and sets the response status code accordingly.
    :param ex: an auth error
    :return: json serialized ex response
    """
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response


def get_token_auth_header() -> str:
    """Obtains the access token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError({"code": "authorization_header_missing",
                         "description":
                             "Authorization header is expected"}, 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError({"code": "invalid_header",
                        "description":
                            "Authorization header must start with"
                            " Bearer"}, 401)
    if len(parts) == 1:
        raise AuthError({"code": "invalid_header",
                        "description": "Token not found"}, 401)
    if len(parts) > 2:
        raise AuthError({"code": "invalid_header",
                         "description":
                             "Authorization header must be"
                             " Bearer token"}, 401)

    token = parts[1]
    return token


def requires_scope(required_scope: str) -> bool:
    """Determines if the required scope is present in the access token
    Args:
        required_scope (str): The scope required to access the resource
    """
    token = get_token_auth_header()
    unverified_claims = jwt.get_unverified_claims(token)
    if unverified_claims.get("scope"):
        token_scopes = unverified_claims["scope"].split()
        for token_scope in token_scopes:
            if token_scope == required_scope:
                return True
    return False


def requires_auth(func):
    """Determines if the access token is valid
    """
    
    @wraps(func)
    def decorated(*args, **kwargs):
        token = get_token_auth_header()
        jsonurl = urlopen("https://" + AUTH0_DOMAIN + "/.well-known/jwks.json")
        jwks = json.loads(jsonurl.read())
        try:
            unverified_header = jwt.get_unverified_header(token)
        except jwt.JWTError as jwt_error:
            raise AuthError({"code": "invalid_header",
                            "description":
                                "Invalid header. "
                                "Use an RS256 signed JWT Access Token"}, 401) from jwt_error
        if unverified_header["alg"] == "HS256":
            raise AuthError({"code": "invalid_header",
                             "description":
                                 "Invalid header. "
                                 "Use an RS256 signed JWT Access Token"}, 401)
        rsa_key = {}
        for key in jwks["keys"]:
            if key["kid"] == unverified_header["kid"]:
                rsa_key = {
                    "kty": key["kty"],
                    "kid": key["kid"],
                    "use": key["use"],
                    "n": key["n"],
                    "e": key["e"]
                }
        if rsa_key:
            try:
                payload = jwt.decode(
                    token,
                    rsa_key,
                    algorithms=ALGORITHMS,
                    audience=API_IDENTIFIER,
                    issuer="https://" + AUTH0_DOMAIN + "/"
                )
            except jwt.ExpiredSignatureError as expired_sign_error:
                raise AuthError({"code": "token_expired",
                                "description": "token is expired"}, 401) from expired_sign_error
            except jwt.JWTClaimsError as jwt_claims_error:
                raise AuthError({"code": "invalid_claims",
                                "description":
                                    "incorrect claims,"
                                    " please check the audience and issuer"}, 401) from jwt_claims_error
            except Exception as exc:
                raise AuthError({"code": "invalid_header",
                                "description":
                                    "Unable to parse authentication"
                                    " token."}, 401) from exc

            _request_ctx_stack.top.current_user = payload
            return func(*args, **kwargs)
        raise AuthError({"code": "invalid_header",
                         "description": "Unable to find appropriate key"}, 401)

    return decorated


def logins_list():
    logins = Logins.query.all()
    logins_schema = LoginsSchema(many=True)
    return logins_schema.dumps(logins)

def logins_get(username):
    logins = Logins.query.filter(Logins.username == username).one_or_none()
    if logins is not None:

        logins_schema = LoginsSchema()
        return logins_schema.dumps(logins)
    else:
        abort(
            404,
            "Login not found for username: {username}".format(username=username),
        )

def logins_post_auth(username, signature):
    schema = LoginsSchema()
    existing_login = (
        Logins.query.filter(Logins.username == username)
        .one_or_none()
    )

    if not existing_login:
        # create entry
        data = {
            "username": username,
            "signature": signature,
            "last_auth": int(time.time()),
            "auth_count": 1,
            "login_count": 0,
        }
        new_login = schema.load(data, session=db.session)
        db.session.add(new_login)
        db.session.commit()
        return schema.dump(new_login)
        
    else:
        # update entry
        data = {
            "id": existing_login.id,
            "signature": signature,
            "last_auth": int(time.time()),
            "auth_count": existing_login.auth_count + 1
        }
        updated_login = schema.load(data, session=db.session)
        db.session.merge(updated_login)
        db.session.commit()
        return schema.dump(updated_login)

def logins_post_login(username):
    schema = LoginsSchema()
    existing_login = (
        Logins.query.filter(Logins.username == username)
        .one_or_none()
    )

    if not existing_login:
        abort(
            404,
            "Login not found for username: {username}".format(username=username),
        )
    else:
        # update entry
        data = {
            "id": existing_login.id,
            "last_login": int(time.time()),
            "login_count": existing_login.login_count + 1
        }
        updated_login = schema.load(data, session=db.session)
        db.session.merge(updated_login)
        db.session.commit()
        return schema.dump(updated_login)

# retrieve a list of all logins table entries
@APP.route("/api/logins")
@cross_origin(headers=["Content-Type", "Authorization"])
@cross_origin(headers=["Access-Control-Allow-Origin", "*"])
@requires_auth
def logins():
    if requires_scope("database:read"):
        return logins_list()
    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)

# retrieve logins entry for specific user
@APP.route("/api/logins/<username>")
@cross_origin(headers=["Content-Type", "Authorization"])
@cross_origin(headers=["Access-Control-Allow-Origin", "*"])
@requires_auth
def logins_by_user(username):
    if requires_scope("database:read"):
        return logins_get(username)
    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)

# authentication endpoint, executed by the authentication gateway
# or game server
@APP.route("/api/logins/<username>", methods=["POST"])
@cross_origin(headers=["Content-Type", "Authorization"])
@cross_origin(headers=["Access-Control-Allow-Origin", "*"])
@requires_auth
def logins_auth_by_user(username):
    data = request.get_json()

    # ugly assignment hack. use "has" etc instead for proper implementation
    try:
        req_type = data["type"]
        signature = data["signature"]
    except:
        pass

    try:
        if not req_type:
            raise Exception("Invalid or missing request type")

        if req_type == "auth":
            if not signature:
                raise Exception("Invalid or missing signature")
        
    except Exception as e:
        abort(
            400,
            e
        )

    if requires_scope("database:write"):
        if req_type == "auth":
            return logins_post_auth(username, signature)
        elif req_type == "login":
            return logins_post_login(username)
    raise AuthError({
        "code": "Unauthorized",
        "description": "You don't have access to this resource"
    }, 403)


if __name__ == "__main__":
    APP.run(host="0.0.0.0", port=env.get("PORT", 3010))
