extends Node
#class_name MYCLASS


##
# export vars
##

##
# script vars
##

var auth0 : Auth0

var access_token : String

##
# signals
##

signal database_connection_ready
signal database_ready

signal get_user_failed
signal get_user_succeeded
signal upsert_user_failed
signal upsert_user_succeeded

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	Logger.debug(self, "ready function started")
	
	# initialize auth0 and jwt class
	auth0 = Auth0.new(Config.auth0_godot_database["domain"], Config.auth0_godot_database["client_id"], Config.auth0_godot_database["client_secret"])
	auth0.audience = Config.auth0_godot_database["audience"]
	auth0.scope = Config.auth0_godot_database["scope"]
	add_child(auth0)
	
	# wait until Auth0 class is ready
	yield(auth0,"auth0_ready")
	
	# retrieve access token for database api 
	auth0.connect("machine_login_failed", self, "_machine_login_failed")
	auth0.connect("machine_login_succeeded", self, "_machine_login_succeeded")
	auth0.login_machine2machine()
	yield(self, "database_connection_ready")
	emit_signal("database_ready")
	# enter infinite loop to retrieve access token regularly
	# as we dont have a renew token we need to get a fresh access 
	# token once every few hours (default lifetime is 24h)
	while true:
		yield(get_tree().create_timer(14400.0), "timeout")
		auth0.login_machine2machine()
		

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _machine_login_failed(result: Dictionary) -> void:
	Logger.error(self, "Unable to get access token for db access")

func _machine_login_succeeded(result: Dictionary) -> void:
	access_token = result["access_token"]
	emit_signal("database_connection_ready")

func _http_request_completed_get_login_for_user(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray, rpc_id: int) -> void:
	
	if response_code != 200:
		emit_signal("get_user_failed", rpc_id)
		return

	var j : JSONParseResult = JSON.parse(body.get_string_from_utf8())
	if j.error != OK:
		Logger.error(self, "Invalid json received")
		emit_signal("get_user_failed", rpc_id)
		return
	
	emit_signal("get_user_succeeded", j.result, rpc_id)

func _http_request_completed_upsert_for_user(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray, rpc_id: int) -> void:
	
	if response_code != 200:
		emit_signal("upsert_user_failed", rpc_id)
		return
	
	emit_signal("upsert_user_succeeded", rpc_id)

##
# script functions
##

func get_login_for_user(rpc_id: int) -> void:
	
	var username : String = PeerData.get_username(rpc_id)
	var url : String = "{api}/logins/{username}".format({"api": Config.app["api_url"],"username": username})
	Logger.debug(self, "calling {url}".format({"url": url}))
	
	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_get_login_for_user", [rpc_id])

	var error = http.request(
		url,
		["Content-Type: application/json", "Authorization: Bearer {t}".format({"t":access_token})],
		true,
		HTTPClient.METHOD_GET,
		""
	)
	if error != OK:
		push_error("An error occurred in the HTTP request.")

	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)
	
func upsert_auth_for_user(rpc_id: int) -> void:
	# update database with user auth info
	# forward auth0 result and rpc id to next signal
	var username : String = PeerData.get_username(rpc_id)
	var signature : String  = PeerData.get_signature(rpc_id)
	var url : String = "{api}/logins/{username}".format({"api": Config.app["api_url"],"username": username})
	Logger.debug(self, "calling {url}".format({"url": url}))
	
	var payload : Dictionary = {
		"type": "auth",
		"signature": signature,
	}

	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_upsert_for_user", [rpc_id])

	var error = http.request(
		url,
		["Content-Type: application/json", "Authorization: Bearer {t}".format({"t":access_token})],
		false,
		HTTPClient.METHOD_POST,
		JSON.print(payload)
	)	
	if error != OK:
		push_error("An error occurred in the HTTP request.")

	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)
	
func upsert_login_for_user(rpc_id: int) -> void:
	# update database with user auth info
	# forward auth0 result and rpc id to next signal
	var username : String = PeerData.get_username(rpc_id)
	var url : String = "{api}/logins/{username}".format({"api": Config.app["api_url"],"username": username})
	Logger.debug(self, "calling {url}".format({"url": url}))
	
	var payload : Dictionary = {
		"type": "login",
	}

	var http = HTTPRequest.new()
	add_child(http,true)
	http.connect("request_completed", self, "_http_request_completed_upsert_for_user", [rpc_id])

	var error = http.request(
		url,
		["Content-Type: application/json", "Authorization: Bearer {t}".format({"t":access_token})],
		false,
		HTTPClient.METHOD_POST,
		JSON.print(payload)
	)	
	if error != OK:
		push_error("An error occurred in the HTTP request.")

	# remove http client after request is completed
	yield(http,"request_completed")
	remove_child(http)
	
