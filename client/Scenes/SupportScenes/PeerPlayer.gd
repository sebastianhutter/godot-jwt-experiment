extends PlayerClass
class_name PeerPlayer


##
# export vars
##


##
# script vars
##

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	_module_color()

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func _module_color() -> void:
	# module color of the peer players mouse pointer
	
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var r : float = rng.randf_range(0.0, 1.0)
	rng.randomize()
	var g : float = rng.randf_range(0.0, 1.0)
	rng.randomize()
	var b : float = rng.randf_range(0.0, 1.0)
	var c : Color = Color(r, g, b, 1.0)
	
	pointer.modulate = c
