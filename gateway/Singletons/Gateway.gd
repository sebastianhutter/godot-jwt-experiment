extends Node
#class_name Gateway


##
# export vars
##

##
# script vars
##

var network : NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
var gateway_api : MultiplayerAPI = MultiplayerAPI.new()
var auth0 : Auth0

var cert = load("res://Resources/Certificates/gateway.crt")
var key = load("res://Resources/Certificates/gateway.key")

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	Logger.debug(self, "ready function started")
	# initialize auth0 and jwt class
	auth0 = Auth0.new(Config.auth0_godot_user["domain"], Config.auth0_godot_user["client_id"], Config.auth0_godot_user["client_secret"])
	auth0.audience = Config.auth0_godot_user["audience"]
	auth0.scope = Config.auth0_godot_user["scope"]
	auth0.database_connection = Config.auth0_godot_user["database_connection"]
	add_child(auth0)
	
	# wait until Auth0 class is ready
	yield(auth0,"auth0_ready")
	
	# connect auth0 signals up
	auth0.connect("user_registration_succeeded",self,"_user_registration_succeeded")
	auth0.connect("user_registration_failed",self,"_user_registration_failed")
	auth0.connect("user_login_succeeded",self,"_user_login_succeeded")
	auth0.connect("user_login_failed",self,"_user_login_failed")

	# connect database signals up
	Database.connect("upsert_user_failed", self, "_upsert_user_failed")
	Database.connect("upsert_user_succeeded", self, "_upsert_user_succeeded")

	# wait for database connection to be ready
	yield(Database,"database_ready")

	# setup server
	start_server()

func _process(delta: float) -> void:
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	custom_multiplayer.poll()
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _peer_connected(id: int) -> void:
	Logger.debug(self, "User {id} connected".format({"id": str(id)}))
	
	PeerData.add_peer(id)
	
func _peer_disconnected(id: int) -> void:
	Logger.debug(self, "User {id} disconnected".format({"id": str(id)}))
	
	PeerData.erase_peer(id)

func _user_registration_succeeded(result: Dictionary, rpc_id: int) -> void:
	Logger.debug(self, "Registration succeeded for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_user_registration_succeeded", result, Rpc.CODE.OK)

func _user_registration_failed(result: Dictionary, rpc_id: int) -> void:
	Logger.warning(self, "Registration failed for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_user_registration_failed", result, Rpc.CODE.REGISTRATION_FAILED)

func _user_login_succeeded(result: Dictionary, rpc_id: int) -> void:
	Logger.debug(self, "Login succeeded for player {id}".format({"id":rpc_id}))
	
	# update player data singleton and database
	PeerData.add_username(rpc_id, auth0.jwt.get_value_from_payload(result["id_token"], "nickname"))
	PeerData.add_access_token(rpc_id, result["access_token"])
	PeerData.add_id_token(rpc_id, result["id_token"])
	PeerData.add_signature(rpc_id, auth0.jwt.get_signature_from_token(result["access_token"]))
	
	Database.upsert_auth_for_user(rpc_id)

func _user_login_failed(result: Dictionary, rpc_id: int) -> void:
	Logger.warning(self, "Login failed for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_user_login_failed", result, Rpc.CODE.AUTHENTICATION_FAILED)
	network.disconnect_peer(rpc_id)

func _upsert_user_failed(rpc_id: int):
	# send a failed login request back to the user, even though the auth0 login
	# worked but the database operation failed
	# we send a non standard http status code back which can be used by the client
	# to print a more meaningful error message

	Logger.warning(self, "Upsert database failed for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_user_login_failed", {}, Rpc.CODE.DATABASE_UPSERT_FAILED)
	network.disconnect_peer(rpc_id)

func _upsert_user_succeeded(rpc_id: int):
	# send a successfull login request back to the user after upsert of db entry 
	# has completed

	var result : Dictionary = {
		"access_token": PeerData.get_access_token(rpc_id),
		"id_token": PeerData.get_id_token(rpc_id),
	}
	Logger.debug(self, "Upsert database succeeded for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_user_login_succeeded", result, Rpc.CODE.OK)
	network.disconnect_peer(rpc_id)

##
# script functions
##

func start_server() -> void:
	network.set_dtls_enabled(true)
	network.set_dtls_key(key)
	network.set_dtls_certificate(cert)
	network.create_server(Config.server["port"], Config.server["max_players"])
	set_custom_multiplayer(gateway_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	Logger.info(self, "Authentication gateway started")
	
	network.connect("peer_connected", self, "_peer_connected")
	network.connect("peer_disconnected", self, "_peer_disconnected")


master func rpc_request_login(username: String, password: String) -> void:
	# login a user via auth0
	
	var player_id = custom_multiplayer.get_rpc_sender_id()
	Logger.debug(self, "login request received for {id}".format({"id":str(player_id)}))
	auth0.login_user(username, password, player_id)

master func rpc_request_registration(username: String, password: String, email: String) -> void:
	# register a new user account
	var player_id = custom_multiplayer.get_rpc_sender_id()
	Logger.debug(self, "registration request received for {id}".format({"id":str(player_id)}))
	auth0.register_user(username, email, password, player_id)
