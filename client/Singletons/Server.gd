extends Node
#class_name Server


##
# export vars
##

##
# script vars
##

var network : NetworkedMultiplayerENet
var server_api : MultiplayerAPI

var cert = load("res://Resources/Certificates/server.crt")

##
# signals
##

signal connection_failed
signal request_access_failed
signal request_access_succeeded

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass
	
func _process(delta: float) -> void:
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	custom_multiplayer.poll()
	
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _connection_failed() -> void:
	emit_signal("connection_failed", "Unable to connect to game server. Please try again later")
	
func _connection_succeeded() -> void:
	request_access()
	
	
##
# script functions
##

func connect_to_server() -> void:
	network = NetworkedMultiplayerENet.new()
	network.set_dtls_enabled(true)
	network.set_dtls_certificate(cert)
	network.set_dtls_verify_enabled(false)
	server_api = MultiplayerAPI.new()
	
	network.create_client(Config.server["ip"], Config.server["port"])
	set_custom_multiplayer(server_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	network.connect("connection_failed", self, "_connection_failed")
	network.connect("connection_succeeded", self, "_connection_succeeded")

func request_access() -> void:
	Logger.debug(self, "Request access to the game server")
	rpc_id(1, "rpc_request_acess", PlayerData.access_token, PlayerData.id_token)

puppet func rpc_request_access_succeeded(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received request access succeeded call")
	emit_signal("request_access_succeeded", result, code)

puppet func rpc_request_access_failed(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received request access failed call")
	emit_signal("request_access_failed", result, code)
	network.disconnect("connection_failed",self,"_connection_failed")
	network.disconnect("connection_succeeded",self,"_connection_succeeded")
