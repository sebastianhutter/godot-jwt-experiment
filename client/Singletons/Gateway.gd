extends Node
#class_name MYCLASS


##
# export vars
##

##
# script vars
##

var network : NetworkedMultiplayerENet
var gateway_api : MultiplayerAPI

var username : String
var password : String
var email : String

var cert = load("res://Resources/Certificates/gateway.crt")

##
# signals
##

signal connection_failed
signal login_succeeded
signal login_failed
signal registration_succeeded
signal registration_failed

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass
	
func _process(delta: float) -> void:
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	custom_multiplayer.poll()
	
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _connection_failed():
	emit_signal("connection_failed", "Unable to connect to login server. Please try again later")
	#_disconnect_signals()
	
func _connection_succeeded():
	# if email is specified request registration from gateway
	# server, if only username and password are set request a login
	if email:
		request_registration()
	else:
		request_login()
	
	
##
# script functions
##

func _connect_signals() -> void:
	network.connect("connection_failed", self, "_connection_failed")
	network.connect("connection_succeeded", self, "_connection_succeeded")
	
func _disconnect_signals() -> void:
	network.disconnect("connection_failed",self,"_connection_failed")
	network.disconnect("connection_succeeded",self,"_connection_succeeded")

func connect_to_server(u: String, p: String, e: String = "") -> void:
	username = u
	password = p
	email = e
	
	network = NetworkedMultiplayerENet.new()
	network.set_dtls_enabled(true)
	network.set_dtls_certificate(cert)
	network.set_dtls_verify_enabled(false)
	gateway_api = MultiplayerAPI.new()
	
	network.create_client(Config.gateway["ip"], Config.gateway["port"])
	set_custom_multiplayer(gateway_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	_connect_signals()

func request_login() -> void:
	rpc_id(1, "rpc_request_login", username, password)
	username = ""
	password = ""

func request_registration() -> void:
	rpc_id(1, "rpc_request_registration", username, password, email)
	username = ""
	password = ""
	email = ""

puppet func rpc_user_login_succeeded(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received user login succeeded call")
	emit_signal("login_succeeded", result, code)
	_disconnect_signals()

puppet func rpc_user_login_failed(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received user login failed call")
	emit_signal("login_failed", result, code)
	_disconnect_signals()

puppet func rpc_user_registration_succeeded(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received user registration succeeded call")
	emit_signal("registration_succeeded", result, code)
	_disconnect_signals()

puppet func rpc_user_registration_failed(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Received user registration failed call")
	emit_signal("registration_failed", result, code)
	_disconnect_signals()
