extends Control
#class_name MYCLASS


##
# export vars
##

##
# script vars
##

onready var username_input = $Background/VBoxContainer/Username
onready var password_input = $Background/VBoxContainer/Password
onready var login_button = $Background/VBoxContainer/LoginButton
onready var register_button = $Background/VBoxContainer/RegisterButton
onready var error_message = $Background/VBoxContainer/ErrorMessage

#var registerscreen_scene : PackedScene = preload("res://Scenes/UIScenes/RegisterScreen.tscn")
var registerscreen_scene_path : String = "res://Scenes/UIScenes/RegisterScreen.tscn"
##
# signals
##

signal access_granted

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	login_button.connect("pressed", self, "_login_button_pressed")
	register_button.connect("pressed", self, "_register_button_pressed")
	Gateway.connect("connection_failed", self, "_connection_failed")
	Gateway.connect("login_succeeded", self, "_login_succeeded")
	Gateway.connect("login_failed", self, "_login_failed")
	Server.connect("connection_failed", self, "_connection_failed")
	Server.connect("request_access_failed", self, "_request_access_failed")
	Server.connect("request_access_succeeded", self, "_request_access_succeeded")

	# set username and password to dotenv values, if not set by other sources
	if username_input.text == "":
		username_input.text = Config.app["username"]
	if password_input.text == "":
		password_input.text = Config.app["password"]

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _login_button_pressed() -> void:
	error_message.text = ""
	if username_input.text == "" and password_input.text == "":
		error_message.text = "please provide valid username and password"
		return
		
	login_button.disabled = true
	register_button.disabled = true
	
	var username : String = username_input.get_text()
	var password : String = password_input.get_text()
	PlayerData.username = username
	
	Gateway.connect_to_server(username, password)

func _register_button_pressed() -> void:
	error_message.text = ""
	login_button.disabled = true
	
	var p : Node = get_parent()
	var s : Resource = load(registerscreen_scene_path)
	p.remove_child(p.get_node("LoginScreen"))
	if p.get_node("LoginScreen"):	
		p.get_node("LoginScreen").call_deferred("free")
	p.add_child(s.instance())

func _connection_failed(error: String) -> void:
	login_button.disabled = false
	register_button.disabled = false
	error_message.text = error
	
func _login_succeeded(result: Dictionary, code: int) -> void:
	Logger.debug(self, "Login succeeded, verifying and storing access tokens")	
	if (
			not result.has("access_token")
			or not result.has("id_token")
		):
		Logger.error(self, "invalid jwt token received")
		error_message.text = "Invalid response from authentication server"
		return
	PlayerData.access_token = result["access_token"]
	PlayerData.id_token = result["id_token"]
	login_button.disabled = false

	# start connecting to server
	Logger.debug(self, "Request access to game server")
	Server.connect_to_server()

func _login_failed(result: Dictionary, code: int) -> void:
	
	error_message.text = Rpc.message(code)
	
	login_button.disabled = false
	register_button.disabled = false
	
func _request_access_failed(result: Dictionary, code: int) -> void:
	
	error_message.text = Rpc.message(code)
	
	login_button.disabled = false
	register_button.disabled = false
	
func _request_access_succeeded(result: Dictionary, code: int) -> void:
	# login and access request succesfull, load into game
	
	emit_signal("access_granted")
	
	
##
# script functions
##
