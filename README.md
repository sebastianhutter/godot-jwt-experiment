# godot-jwt-experiment

Simple authorative multiplayer experiment using auth0 and jwt to register and authenticate users and allow backend systems to communicate.

The game itself is simple (it's not really a game ;-) ).

After connecting to the game server the user can "spawn" stars with clicking of the left mouse button and remove stars by clicking on them with the right mouse button. Every connected player gets a randomly choosen color.

## Overview

![godot jwt experiment overview](overview.png)

## Components

The project is made up of multiple components
- [auth0](https://auth0.com/) (cloud service) - user directory and oauth2 provider
- [client](./client) - the godot game client
- [database](./database) - mysql:8 service containing a single table to store login meta information for clients
- [database-api](./database-api) - rest api providing access to the mysql database via http calls
- [gateway](./gateway) - the godot gateway responsible for user login
- [server](./server) - the godot gameserver

## Requirements

- ***godot 3.4 beta***
- docker
- auth0 account

## Preparation

### submodules

The git repository uses [Classes](https://gitlab.com/sebastianhutter/godot-classes) from a different git repository. You need to download the git submodules before running the program

```bash
git submodule init
git submodule update --remote --merge
```

### Auth0

The demo uses [auth0](https://auth0.com/) as a user backend. 

**Create a database connection**
We use a custom user database for the godot users.

- Navigate to "Authentication" -> "Database" and create a new database named `godot`
- Set the flag "Requires username" to `true`
- Navigate to "Settings" -> "General" and set the "Default Directory" in "API Authorizaton Settings" to `godot`

**Create new APIs**
APIs in auth0 are required to enable machine to machine communication and allow access for specific endpoints

- Navigate to "Applications" -> "APIs"
- Create two new APIs : `godot-user` and `godot-database` (use the same value for name and identifier)
- Select the API `godot-user`, open "Permissions", add one new permission `login:user`.
- Select the API `godot-database`, open "Permissions", add two new permissions `database:read` and `database:write`

**Create new client application**
The next step is to create a client application which the godot gateway can use to authenticate users.

- Navigate to "Applications" and hit "Create application"
- Use `godot-user` as name for the application
- Choose "Regular Web Applications" as type
- Goto "Settings" of the new application, open "Advanced Settings" -> "Grant Types" and enable the "Password" grant type
- Goto "Connections" of the new application and enable the Database connection for `godot`. Disable all other connections

**Create a new database application**
The database application is used by the godot gatewat and godot server to communicate with the database api.

- Navigate to "Applications" and hit "Create application"
- Use `godot-database` as name for the application
- Choose "Machine to Machine applications" as type
- Select the API `godot-database` and hit "Authorize"
- Navigate back to "Applications" -> "APIs" and open the `godot-database` api. 
  - Goto "Machine to Machine Applications", open the details for the `godot-database` application and allow the two permissions `database:read` and `database:write`

### Environment File

The next step is to create the environment 

Copy the `.env.example` file to `.env` and add the values from auth0.

## Usage

With everyting in place you can build and start all backend services with the [docker-compose.yaml](./docker-compose.yaml) file.

```bash
docker-compose up
```

With the backend service running you can start the godot game [client](./client) with the godot 3.4 editor and run the project.

If you are running the docker-compose the first time the build process will take some time to complete!
## Notes

### certificates

To tes the encrypted traffic between client and gateway and other services the repository is using self signed certificates.
**As the private key is checked into the repo please do not reuse the certificates for anything but demo purposes!**

### mysql database access

The docker-compose file starts an adminer service which is accessible via http://localhost:8080.
adminer can be used to access the database. Enter `godot` for username, password and the database.
