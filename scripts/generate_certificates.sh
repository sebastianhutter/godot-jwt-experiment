#!/bin/bash

#
# generate self signed certificates
#

[ ! -d ".git" ] && echo "please execute script from git root" && exit 1

# create gateway certificate
openssl req -nodes -new -x509 -subj "/C=CH/ST=ZH/L=Winterthur/O=godot/OU=jwt-multiplayer" \
    -keyout ./gateway/Resources/Certificates/gateway.key -out ./gateway/Resources/Certificates/gateway.crt -days 3650
cp ./gateway/Resources/Certificates/gateway.crt ./client/Resources/Certificates/gateway.crt

# create server certificate
openssl req -nodes -new -x509 -subj "/C=CH/ST=ZH/L=Winterthur/O=godot/OU=jwt-multiplayer" \
    -keyout ./server/Resources/Certificates/server.key -out ./server/Resources/Certificates/server.crt -days 3650
cp ./server/Resources/Certificates/server.crt ./client/Resources/Certificates/server.crt

