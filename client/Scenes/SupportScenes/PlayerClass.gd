extends Node2D
class_name PlayerClass


##
# export vars
##

##
# script vars
##

onready var username_label : Label = $Username
onready var pointer : Sprite = $Pointer

##
# signals
##

##
# default functions
##

func _init() -> void:
	if custom_multiplayer == null:
		custom_multiplayer = Server.custom_multiplayer

func _ready() -> void:
	_initialize_player()
	
func _process(delta: float) -> void:
	pass

func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func _initialize_player() -> void:
	# retrieve information about player object from server
	rpc_id(1, "rpc_initialize_player")

func set_username(s: String) -> void:
	# set the username label string
	username_label.text = s

puppet func rpc_update_position(pos: Vector2) -> void:
	# update the current position, received from game server
	position = pos
	#_move_namebox(pos)

puppet func rpc_update_username(s: String) -> void:
	# receive the username for the player
	
	set_username(s)
