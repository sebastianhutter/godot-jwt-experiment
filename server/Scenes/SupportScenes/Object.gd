extends Node2D
class_name ObjectStar


##
# export vars
##

onready var area2d : Area2D = $Area2D
onready var collision : CollisionShape2D = $Area2D/CollisionShape2D

##
# script vars
##

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

