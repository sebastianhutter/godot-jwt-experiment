#!/bin/bash

#
# script to get a user entry from the database api
#

[ ! -d ".git" ] && echo "please execute script from git root" && exit 1

[ ! -f ".env" ] && echo "unable to find .env file. aborting" && exit 1

username="${1}"
[ -z "${username}" ] && echo "please specify a username" && exit 1


# load .env file
source .env

# get access token from auth0
token_url=https://${AUTH0_DOMAIN}/oauth/token
payload="{\"grant_type\":\"client_credentials\",\"client_id\":\"${AUTH0_CLIENT_ID_GODOT_DATABASE}\",\"client_secret\":\"${AUTH0_CLIENT_SECRET_GODOT_DATABASE}\",\"audience\":\"${AUTH0_AUDIENCE_GODOT_DATABASE}\",\"scope\":\"${AUTH0_SCOPE_GODOT_DATABASE}\"}"
access_token=$(curl --silent --request POST \
  --url ${token_url} \
  --header 'content-type: application/json' \
  --data "${payload}" | jq -r .access_token)

# get user from database
curl -XGET http://localhost:3010/api/logins/${username} --header 'content-type: application/json' --header "Authorization: Bearer ${access_token}"
