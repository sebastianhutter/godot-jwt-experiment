extends Node
#class_name Server


##
# export vars
##

##
# script vars
##

var network : NetworkedMultiplayerENet = NetworkedMultiplayerENet.new()
var server_api : MultiplayerAPI = MultiplayerAPI.new()
var auth0 : Auth0

var cert = load("res://Resources/Certificates/server.crt")
var key = load("res://Resources/Certificates/server.key")

##
# signals
##

signal server_ready
signal spawn_player
signal despawn_player

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	Logger.debug(self, "ready function started")
	# initialize auth0 and jwt class
	# the server only needs to verify given jwt tokens. no client id and secret is required
	auth0 = Auth0.new(Config.auth0_godot_user["domain"])
	add_child(auth0)
	
	# wait until Auth0 class is ready
	yield(auth0,"auth0_ready")

	# connect database signals up
	Database.connect("upsert_user_failed", self, "_upsert_user_failed")
	Database.connect("upsert_user_succeeded", self, "_upsert_user_succeeded")
	Database.connect("get_user_failed", self, "_get_user_failed")
	Database.connect("get_user_succeeded", self, "_get_user_succeeded")

	# wait for database connection to be ready
	yield(Database,"database_ready")

	# setup server
	start_server()

func _process(delta: float) -> void:
	if get_custom_multiplayer() == null:
		return
	if not custom_multiplayer.has_network_peer():
		return
	custom_multiplayer.poll()
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _peer_connected(rpc_id: int) -> void:
	Logger.debug(self, "User {id} connected".format({"id": str(rpc_id)}))

	PeerData.add_peer(rpc_id)
	
func _peer_disconnected(rpc_id: int) -> void:
	Logger.debug(self, "User {id} disconnected".format({"id": str(rpc_id)}))
	
	# send despawn player signal to world
	emit_signal("despawn_player", rpc_id)
	# remove player info from peer object
	PeerData.erase_peer(rpc_id)

func _upsert_user_failed(rpc_id: int):
	# send a failed access request back to the user, even though the auth0 login
	# worked but the database operation failed
	# we send a non standard http status code back which can be used by the client
	# to print a more meaningful error message

	Logger.warning(self, "Upsert database failed for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.CODE.DATABASE_UPSERT_FAILED)
	network.disconnect_peer(rpc_id)

func _upsert_user_succeeded(rpc_id: int):
	# send a successfull access request back to the user after upsert of db entry 
	# has completed

	Logger.debug(self, "Upsert database succeeded for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_request_access_succeeded", {}, Rpc.CODE.OK)
	
	# send spawn player signal to world
	emit_signal("spawn_player", rpc_id)

func _get_user_failed(rpc_id: int) -> void:
	# unable to retrieve user info from database
	
	Logger.warning(self, "Get user login info failed for player {id}".format({"id":rpc_id}))
	rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.CODE.DATABSE_GET_FAILED)
	network.disconnect_peer(rpc_id)

func _get_user_succeeded(result: Dictionary, rpc_id: int) -> void:
	# user info was successfully retrieved
	# verify given signature and timestamps
	Logger.debug(self, "Get user info succeeded for player {id}".format({"id":rpc_id}))

	# first make sure the signature stored in the databse
	# is equal to the signature received by the peer
	if not result.has("signature") or not result.has("last_auth") or not result.has("last_login"):
		Logger.warning(self, "Invalid response received from database for user {id}".format({"id":str(rpc_id)}))
		rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.CODE.DATABSE_GET_FAILED)
		network.disconnect_peer(rpc_id)
		return
		
	if not PeerData.get_signature(rpc_id) == result["signature"]:
		Logger.warning(self, "Received signature doesnt match signature stored in database for user {id}".format({"id":str(rpc_id)}))
		rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.CODE.AUTHENTICATION_FAILED)
		network.disconnect_peer(rpc_id)
		return
		
	# next make sure the user hasnt already logged in once with the 
	# given signature. this is done by comparing "last_auth" and "last_login"
	# timestamps retrieved from the database
	if result["last_login"] > result["last_auth"]:
		Logger.warning(self, "last_login timestamp newer then last_auth timestamp for user {id}".format({"id":str(rpc_id)}))
		rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.CODE.AUTHENTICATION_FAILED)
		network.disconnect_peer(rpc_id)
		return

	# pretty sure that the user shall be given access
	# lets upsert the database with the current timestamp for the last login
	# and the login count
	Database.upsert_login_for_user(rpc_id)

##
# script functions
##

func start_server() -> void:
	network.set_dtls_enabled(true)
	network.set_dtls_key(key)
	network.set_dtls_certificate(cert)
	network.create_server(Config.server["port"], Config.server["max_players"])
	set_custom_multiplayer(server_api)
	custom_multiplayer.set_root_node(self)
	custom_multiplayer.set_network_peer(network)
	
	Logger.info(self, "Game server started")
	emit_signal("server_ready")
	
	network.connect("peer_connected", self, "_peer_connected")
	network.connect("peer_disconnected", self, "_peer_disconnected")


master func rpc_request_acess(access_token: String, id_token: String) -> void:
	# access request by client
	# verify validity of access and id token
	# verify stored signature of login process (in db)
	# allow access depending on validated signatures
	
	var rpc_id = custom_multiplayer.get_rpc_sender_id()
	Logger.debug(self, "access request received for {id}".format({"id":str(rpc_id)}))

	# first validate the access and id token
	if not auth0.jwt.verify_token(access_token):
		Logger.debug(self, "invalid access token received for {id}".format({"id":str(rpc_id)}))
		rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.RCP_CODE.INVALID_TOKEN)
	if not auth0.jwt.verify_token(id_token):
		Logger.debug(self, "invalid id token received for {id}".format({"id":str(rpc_id)}))
		rpc_id(rpc_id, "rpc_request_access_failed", {}, Rpc.RCP_CODE.INVALID_TOKEN)

	# update player data singleton and request info from database
	PeerData.add_username(rpc_id, auth0.jwt.get_value_from_payload(id_token, "nickname"))
	# we dont need to store the id and access token on the server
	# nor should we as they belong to the player
	#PeerData.add_access_token(rpc_id, access_token)
	#PeerData.add_id_token(rpc_id, id_token)
	PeerData.add_signature(rpc_id, auth0.jwt.get_signature_from_token(access_token))

	# received tokens are valid
	# next verify that the given signature of the tokens 
	# is the same as registered in the database
	Database.get_login_for_user(rpc_id)

