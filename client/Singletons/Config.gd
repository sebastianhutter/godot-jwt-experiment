extends Node
#class_name Config


##
# export vars
##

##
# script vars
##


var server : Dictionary = {
}

var gateway : Dictionary = {
}

var app : Dictionary = {
}

##
# signals
##

##
# default functions
##

func _init() -> void:

	DotEnv.load("../.env")

	server["ip"] =  DotEnv.envString("SERVER_IP", "127.0.0.1")
	server["port"] = DotEnv.envInt("SERVER_PORT", 1909)
	gateway["ip"] =  DotEnv.envString("GATEWAY_IP", "127.0.0.1")
	gateway["port"] = DotEnv.envInt("GATEWAY_PORT", 1910)
	
	app["username"] = DotEnv.envString("USERNAME", "")
	app["password"] = DotEnv.envString("PASSWORD", "")
	
	app["loglevel"] = Logger.get_loglevel(DotEnv.envString("LOGLEVEL", "info"))

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##
