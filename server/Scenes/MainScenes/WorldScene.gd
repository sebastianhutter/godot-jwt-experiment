extends Node2D
class_name WorldScene


##
# export vars
##

##
# script vars
##

onready var players_node : Node = $Players
onready var objects_node : Node = $Objects

onready var player_scene : PackedScene = preload("res://Scenes/SupportScenes/Player.tscn")
onready var object_scene : PackedScene = preload("res://Scenes/SupportScenes/Object.tscn")

##
# signals
##

##
# default functions
##

func _init() -> void:
	yield(Server, "server_ready")
	if custom_multiplayer == null:
		custom_multiplayer = Server.custom_multiplayer

func _ready() -> void:
	Server.connect("despawn_player", self, "_despawn_player")
	Server.connect("spawn_player", self, "_spawn_player")

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _despawn_player(rpc_id: int) -> void:
	Logger.debug(self, "Received despawn for player {id}".format({"id":rpc_id}))
	_deinstance_player(rpc_id)

func _spawn_player(rpc_id: int) -> void:
	Logger.debug(self, "Received spawn for player {id}".format({"id":rpc_id}))
	_instance_player(rpc_id)

func _player_left_mouseclick(rpc_id: int, pos: Vector2) -> void:
	# handle left mouse clicks on world 
	
	Logger.debug(self, "Received left click event for {id}".format({"id":rpc_id}))
	_instance_player_object(rpc_id, pos)
	

func _player_right_mouseclick(rpc_id: int, pos: Vector2) -> void:
	# handle right mouse clicks on world 
	
	Logger.debug(self, "Received right click event for {id}".format({"id":rpc_id}))
	_deinstance_player_object(rpc_id, pos)

##
# script functions
##

func _instance_player(rpc_id: int) -> void:
	# create a player instance and add it to the players node container
	
	if players_node.get_node(str(rpc_id)):
		Logger.warning(self, "player instance for {id} already exists!".format({"id":rpc_id}))
		return
		
	# send spawn player command to all connected peers
	rpc_id(0, "rpc_spawn_player", rpc_id)
	# send spawn player command for existing peers to newly connected peer
	for p in players_node.get_children():
		if int(p.name) != rpc_id:
			rpc_id(rpc_id, "rpc_spawn_player", int(p.name))
		
	# send spawn object command for existing objects to newly connected peer
	for o in objects_node.get_children():
		if int(o.name) != rpc_id:
			for po in o.get_children(): 
				rpc_id(rpc_id, "rpc_spawn_player_object", int(o.name), po.name, po.position)	
	
	var player : Node2D = player_scene.instance()
	player.name = str(rpc_id)
	players_node.add_child(player,true)
	player.set_username(PeerData.get_username(rpc_id))
	
	# register left and right mouseclick signals
	player.connect("left_mouseclick", self, "_player_left_mouseclick")
	player.connect("right_mouseclick", self, "_player_right_mouseclick")

func _deinstance_player(rpc_id: int) -> void:
	# remove the player object from the tree
	var player : Node2D = players_node.get_node(str(rpc_id))
	var players_objects : Node = objects_node.get_node(str(rpc_id))

	rpc_id(0, "rpc_despawn_player", rpc_id)

	if player:
		player.disconnect("left_mouseclick", self, "_player_left_mouseclick")
		player.disconnect("right_mouseclick", self, "_player_right_mouseclick")
		player.call_deferred("queue_free")
		players_node.remove_child(player)
		
	if players_objects:
		players_objects.call_deferred("queue_free")
		objects_node.remove_child(players_objects)

func _instance_player_object(rpc_id: int, pos: Vector2) -> void:
	# instance an object created by the left click of the mouse player
	
	# first create subnode in objects folder for easy sorting
	if not objects_node.get_node(str(rpc_id)):
		var n : Node = Node.new()
		n.name = str(rpc_id)
		objects_node.add_child(n, true)

	# next instance the object scene
	var object : Node2D = object_scene.instance()
	objects_node.get_node(str(rpc_id)).add_child(object,true)
	object.position = pos
	
	rpc_id(0, "rpc_spawn_player_object", rpc_id, object.name, object.position)
	
func _deinstance_player_object(rpc_id: int, pos: Vector2) -> void:
	# remove a player object on right click
	
	var players_objects : Node = objects_node.get_node(str(rpc_id))
	
	if players_objects:
		# loop trough all players objects
		for o in players_objects.get_children():
			# if any overlapping areas are detected
			# loop trough them and check if the area is the players
			# mouse pointer
			var overlapping_areas : Array = o.area2d.get_overlapping_areas()
			for a in overlapping_areas:
				if int(a.get_parent().name) == rpc_id:
					rpc_id(0, "rpc_despawn_player_object", rpc_id, o.name)
					o.call_deferred("queue_free")
					players_objects.remove_child(o)
