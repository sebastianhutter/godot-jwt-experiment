extends Control
#class_name MYCLASS


##
# export vars
##

##
# script vars
##

onready var username_input = $Background/VBoxContainer/Username
onready var password_input = $Background/VBoxContainer/Password
onready var email_input = $Background/VBoxContainer/Email
onready var login_button = $Background/VBoxContainer/LoginButton
onready var register_button = $Background/VBoxContainer/RegisterButton
onready var error_message = $Background/VBoxContainer/ErrorMessage

#var loginscreen_scene : PackedScene = preload("res://Scenes/UIScenes/LoginScreen.tscn")
var loginscreen_scene_path : String = "res://Scenes/UIScenes/LoginScreen.tscn"

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	login_button.connect("pressed", self, "_login_button_pressed")
	register_button.connect("pressed", self, "_register_button_pressed")
	Gateway.connect("connection_failed", self, "_connection_failed")
	Gateway.connect("registration_succeeded", self, "_registration_succeeded")
	Gateway.connect("registration_failed", self, "_registration_failed")

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

func _login_button_pressed() -> void:
	error_message.text = ""
	login_button.disabled = true
	
	var p : Node = get_parent()
	var s : Resource = load(loginscreen_scene_path)
	p.remove_child(p.get_node("RegisterScreen"))
	if p.get_node("RegisterScreen"):	
		p.get_node("RegisterScreen").call_deferred("free")
	p.add_child(s.instance())

func _register_button_pressed() -> void:
	error_message.text = ""
	if (
		username_input.text == "" 
		or password_input.text == ""
		or email_input.text == ""
		):
		error_message.text = "please provide valid username, password and email"
		return
		
	register_button.disabled = true
	
	var username : String = username_input.get_text()
	var password : String = password_input.get_text()
	var email : String = email_input.get_text()
	
	Gateway.connect_to_server(username, password, email)

func _connection_failed(error: String) -> void:
	register_button.disabled = false
	login_button.disabled = false
	error_message.text = error
	
func _registration_succeeded(result: Dictionary, code: int) -> void:
	# after successfull registration switch
	# back to the login screen
	
	var p : Node = get_parent()
	var s : Control = load(loginscreen_scene_path).instance()
	s.find_node("ErrorMessage").text = "Registration succeeded. Please activate your account (email)."
	s.find_node("Username").text = result["username"] 
	s.find_node("Password").text = ""
	p.remove_child(p.get_node("RegisterScreen"))
	if p.get_node("RegisterScreen"):	
		p.get_node("RegisterScreen").call_deferred("free")
	p.add_child(s)
	
func _registration_failed(result: Dictionary, code: int) -> void:
	error_message.text = Rpc.message(code)
	
	login_button.disabled = false
	register_button.disabled = false


##
# script functions
##

