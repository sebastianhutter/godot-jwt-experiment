extends Node
#class_name Config


##
# export vars
##

##
# script vars
##

var auth0_godot_database : Dictionary = {
}

var auth0_godot_user : Dictionary = {
}

var server : Dictionary = {
}

var app : Dictionary = {
}

##
# signals
##

##
# default functions
##

func _init() -> void:
	
	DotEnv.load("../.env")
	
	# load environment variables and pass them into the dictionaries	
	auth0_godot_database["domain"] = DotEnv.envString("AUTH0_DOMAIN")
	auth0_godot_database["client_id"] = DotEnv.envString("AUTH0_CLIENT_ID_GODOT_DATABASE")
	auth0_godot_database["client_secret"] = DotEnv.envString("AUTH0_CLIENT_SECRET_GODOT_DATABASE")
	auth0_godot_database["audience"] = DotEnv.envString("AUTH0_AUDIENCE_GODOT_DATABASE")
	auth0_godot_database["scope"] = DotEnv.envString("AUTH0_SCOPE_GODOT_DATABASE")
	
	auth0_godot_user["domain"] = DotEnv.envString("AUTH0_DOMAIN")
	
	server["port"] =  DotEnv.envInt("SERVER_PORT", 1909)
	server["max_players"] = DotEnv.envInt("SERVER_MAX_PLAYERS", 100)
	
	app["loglevel"] = Logger.get_loglevel(DotEnv.envString("LOGLEVEL", "info"))
	app["api_url"] = DotEnv.envString("API_URL", "http://localhost:3010/api")


func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##
