extends Node
#class_name PeerData

#
# manager connected peer data
#

##
# export vars
##

##
# script vars
##

var peers : Dictionary 

##
# signals
##

##
# default functions
##

func _init() -> void:
	pass

func _ready() -> void:
	pass

func _process(delta: float) -> void:
	pass
	
func _physics_process(delta: float) -> void:
	pass

##
# getter and setter functions
##

##
# signal functions
##

##
# script functions
##

func add_peer(id: int) -> void:
	# initilaize peer object
	if peers.has(id):
		Logger.warning(self, "peer {id} already exists".format({"id":id}))
		return
	
	peers[id] = {}

func erase_peer(id: int) -> void:
	# remove peer object

	if peers.has(id):
		peers.erase(id)
		
func _update_peer_value(id: int, key: String, value) -> void:
	# add or update key / value pair for peer
	
	if not peers.has(id):
		Logger.warning(self, "peer {id} does not exist".format({"id":id}))
		return
		
	if peers[id].has(key):
		Logger.debug(self, "key {key} for peer {id} exists. overwriting value".format({"key":key,"id":id}))
	
	peers[id][key] = value

func _get_peer_value(id: int, key: String):
	# return the given key value of a peer
	# if no value is found return null
	
	if not peers.has(id):
		Logger.warning(self, "peer {id} does not exist".format({"id":id}))
		return null
	
	if not peers[id].has(key):
		Logger.debug(self, "key {key} for peer {id} does not exists".format({"key":key,"id":id}))
	
	return peers[id].get(key, null)

func add_username(id: int, username: String) -> void:
	# add username to peer object
	
	_update_peer_value(id, "username", username)

func get_username(id: int) -> String:
	# retrieve username from peer object
	
	var username = _get_peer_value(id, "username")
	if not username:
		return ""
	return username

func add_access_token(id: int, access_token: String) -> void:
	# add access_token to peer object
	
	_update_peer_value(id, "access_token", access_token)

func get_access_token(id: int) -> String:
	# retrieve access_token from peer object
	
	var access_token = _get_peer_value(id, "access_token")
	if not access_token:
		return ""
	return access_token

func add_id_token(id: int, id_token: String) -> void:
	# add id_token to peer object
	
	_update_peer_value(id, "id_token", id_token)

func get_id_token(id: int) -> String:
	# retrieve id_token from peer object
	
	var id_token = _get_peer_value(id, "id_token")
	if not id_token:
		return ""
	return id_token
	
func add_signature(id: int, signature: String) -> void:
	# add signature to peer object
	
	_update_peer_value(id, "signature", signature)

func get_signature(id: int) -> String:
	# retrieve id_token from peer object
	
	var signature = _get_peer_value(id, "signature")
	if not signature:
		return ""
	return signature
